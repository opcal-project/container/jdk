#!/bin/sh

set -e

echo " "
echo " "
echo 'build liberica-nik-java11-22 start'

# https://download.bell-sw.com/vm/22.1.0/bellsoft-liberica-vm-openjdk11.0.15+10-22.1.0+1-linux-amd64.tar.gz
GRAALVM_VERSION=22.2.0
NIK_VERSION=bellsoft-liberica-vm-openjdk11.0.16.1+1-22.2.0+3
TARGETPLATFORM=linux-amd64
NIK_FOLDER=bellsoft-liberica-vm-openjdk11-${GRAALVM_VERSION}
BASE_IMAGE=registry.gitlab.com/opcal-project/containers/ubuntu:jammy

# liberica-nik-java11-22
docker build \
    --build-arg BASE_IMAGE=${BASE_IMAGE} \
    --build-arg GRAALVM_VERSION=${GRAALVM_VERSION} \
    --build-arg NIK_VERSION=${NIK_VERSION} \
    --build-arg TARGETPLATFORM=${TARGETPLATFORM} \
    --build-arg NIK_FOLDER=${NIK_FOLDER} \
    -t liberica-nik:java11-22-${TAG_VERSION} \
    -f ${PROJECT_DIR}/native-vm/liberica-nik/base/Dockerfile . --no-cache
docker image tag liberica-nik:java11-22-${TAG_VERSION} ${CONTAINER_REGISTRY_URL}/opcal-project/containers/liberica-nik:java11-22-${TIMESTAMP}
docker image tag liberica-nik:java11-22-${TAG_VERSION} ${CONTAINER_REGISTRY_URL}/opcal-project/containers/liberica-nik:java11-22
docker image tag liberica-nik:java11-22-${TAG_VERSION} ${CONTAINER_REGISTRY_URL}/opcal-project/containers/liberica-nik:java11-${GRAALVM_VERSION}
docker push ${CONTAINER_REGISTRY_URL}/opcal-project/containers/liberica-nik:java11-22-${TIMESTAMP}
docker push ${CONTAINER_REGISTRY_URL}/opcal-project/containers/liberica-nik:java11-22
docker push ${CONTAINER_REGISTRY_URL}/opcal-project/containers/liberica-nik:java11-${GRAALVM_VERSION}

docker rmi -f ${CONTAINER_REGISTRY_URL}/opcal-project/containers/liberica-nik:java11-${GRAALVM_VERSION}
docker rmi -f ${CONTAINER_REGISTRY_URL}/opcal-project/containers/liberica-nik:java11-22
docker rmi -f ${CONTAINER_REGISTRY_URL}/opcal-project/containers/liberica-nik:java11-22-${TIMESTAMP}
docker rmi -f liberica-nik:java11-22-${TAG_VERSION}

echo 'build liberica-nik-java11-22 finished'
echo " "
echo " "
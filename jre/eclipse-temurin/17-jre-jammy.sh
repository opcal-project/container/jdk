#!/bin/sh

set -e

echo " "
echo " "
echo 'build eclipse-temurin-17-jre-jammy start'

IMAGE=eclipse-temurin:17-jre-jammy

# 17-jre-jammy
docker build \
    --build-arg BASE_IMAGE=${IMAGE} \
    -t eclipse-temurin:17-jre-jammy-${TAG_VERSION} \
    -f ${PROJECT_DIR}/jre/eclipse-temurin/base/ubuntu/Dockerfile . --no-cache
docker image tag eclipse-temurin:17-jre-jammy-${TAG_VERSION} ${CONTAINER_REGISTRY_URL}/opcal-project/containers/eclipse-temurin:17-jre-jammy-${TIMESTAMP}
docker image tag eclipse-temurin:17-jre-jammy-${TAG_VERSION} ${CONTAINER_REGISTRY_URL}/opcal-project/containers/eclipse-temurin:17-jre-jammy

docker push ${CONTAINER_REGISTRY_URL}/opcal-project/containers/eclipse-temurin:17-jre-jammy-${TIMESTAMP}
docker push ${CONTAINER_REGISTRY_URL}/opcal-project/containers/eclipse-temurin:17-jre-jammy

## 17-jre
docker image tag eclipse-temurin:17-jre-jammy-${TAG_VERSION} ${CONTAINER_REGISTRY_URL}/opcal-project/containers/eclipse-temurin:17-jre
docker push ${CONTAINER_REGISTRY_URL}/opcal-project/containers/eclipse-temurin:17-jre
docker rmi -f ${CONTAINER_REGISTRY_URL}/opcal-project/containers/eclipse-temurin:17-jre
## 17-jre

docker rmi -f ${CONTAINER_REGISTRY_URL}/opcal-project/containers/eclipse-temurin:17-jre-jammy
docker rmi -f ${CONTAINER_REGISTRY_URL}/opcal-project/containers/eclipse-temurin:17-jre-jammy-${TIMESTAMP}
docker rmi -f eclipse-temurin:17-jre-jammy-${TAG_VERSION}

echo 'build eclipse-temurin-17-jre-jammy finished'
echo " "
echo " "